# AplineHelloworld
* Mise en place d'un pipline CICD avec :
    * GitLab pour la partie CICD
    * Heroku pour la partie dépoiement

# TP1 : Build
* Création d'un projet vide dans GitLab
```bash
 Copie du code alpine et clone dans le nouveau repos gitlab
git clone https://github.com/eazytrainingfr/alpinehelloworld.git
cd alpinehelloworld/
git remote -v 
# help
git remote -h
# on supprimer l'origine
git remote remove origin 
git remote -v 
# on modifier l'origine
git remote add origin git@gitlab.com:omarpiotr/alpinehelloworld-ajc.git
# on pousse le code
git push origin master
```

* Création d'un pipline simple avec uniquement Build dans gitlab-ci.yml

# TP2 : Test d'acceptance
* Modification du pipeline avec un job qui
    * lance l’image buildée
    * teste à l’aide de curl

# TP3 : Release
* Emplacement des images dockers dans la registry de GitLab :
    * Package & Registry → Contenair Registry
        * registry.gitlab.com/omarpiotr/alpinehelloworld-ajc
        * on peut télécharger l'artefactf (dans notre cas ce sont des images docker)

* Ajout d'unve variable d'environnement pour le projet
    * Setting → CI/CD → Variables → Extend → add variable
        * nom : IMAGE_NAME
        * valeur : registry.gitlab.com/omarpiotr/alpinehelloworld-ajc
```
CI_COMMIT_REF_NAME : Branche
CI_COMMIT_SHORT_SHA : Commit
CI_REGISTRY_USER : Login pour se connecter au registre
CI_REGISTRY_PASSWORD  : password du registre
CI_REGISTRY : url du registre
```

# TP4 : Déploiement prod et staging
* Créer un compte chez HEROKU puis générer/ Récupérez la clé :
    * Account setting → API key → CLE_HEROKU...

* Dans Git Lab créer une nouvelle varibale
    * HEROKU_API_KEY
    * CLE_HEROKU...

* heroku refait un rebuild de l'image avant de la publier

# TP5 : Déploimeent de review
* Deploiement d'une review si Merge Request
* Si validation de la Merge Request :
    * suppression de la review
    * déploiment des nouveaux prod et staging

* Etapes :
    * créer une nouvelle branche : welcom
        * 3 job lancé dans ce pipline (les autres ne sont pas lancé car ne concerne pas la branche Master)
    * sur la branche welcome 
        * modifier app.py pour notre test
        * faire un commit (dans la CI toujours 3 jobs)
    * déclanche une webRequest
        * Merge Request (assign) → Delete branche source → create Merge Request
        cela doit créer les environnement de revue (Depoyement)
    * Deployement → Environnement (l'environnement review pour la branche welcom apparait)
    * On valide la Merge Request 
        * le pipline se lance :
            * supprimer l'environnement review
            * met à jour les environnement prod et staging

# TP6 : Template pour factoriser le code



---
An Alpine-based Docker example By EAZYTraining

[![Build Status](http://ip10-0-0-3-bsu2msf734ug0cfcege0-8080.direct.docker.labs.eazytraining.fr/buildStatus/icon?job=deploiement)](http://ip10-0-0-3-bsu2msf734ug0cfcege0-8080.direct.docker.labs.eazytraining.fr/job/deploiement/)
